import numpy as np
import cv2

vid = cv2.VideoCapture('IMG-3607.MOV')
while vid.isOpened():
    ret, frame = vid.read()
    if ret:
        # frame = np.array(frame)
        cvid = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
        circles = cv2.HoughCircles(cvid,cv2.HOUGH_GRADIENT,1,10000,param1=50,param2=50,minRadius=300,maxRadius=400)
    if circles is not None:
        circles = np.round(circles[0, :]).astype("int")
        for (x,y,r) in circles:
            cv2.circle(cvid, (x,y), r, (255,255,255), 2)
            cv2.circle(cvid,(x,y), 2,(255,255,255),3)

        cv2.imshow("detected circles", cvid)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        break

cv2.waitKey(0)
cv2.destroyAllWindows() 
